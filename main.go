package main

import (
	"flag"
	"fmt"
	"os"

	"bget/handler"
)

func main() {

	bgetWrapper := handler.BgetWrapper{}

	flag.StringVar(&bgetWrapper.IP, "i", "", "IP address of bagent")
	flag.IntVar(&bgetWrapper.Port, "p", -1, "port of bagent")
	flag.StringVar(&bgetWrapper.Proxy, "x", "", "IP addr of proxy")
	flag.IntVar(&bgetWrapper.ProxyPort, "P", -1, "port of proxy")
	flag.StringVar(&bgetWrapper.Key, "k", "", "key")
	flag.Parse()

	if len(bgetWrapper.IP) != 0 && bgetWrapper.Port == -1 {
		fmt.Sprintf("the port of bagent should be given")
		os.Exit(1)
	}

	if len(bgetWrapper.Proxy) != 0 && bgetWrapper.ProxyPort == -1 {
		fmt.Sprintf("the port of bagent proxy should be given")
		os.Exit(1)
	}

	if len(bgetWrapper.Key) == 0 {
		fmt.Sprintf("no key, exit")
		os.Exit(1)
	}

	bgetWrapper.Execute()

	os.Exit(0)
}
