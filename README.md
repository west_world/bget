## commit note 2018.05.06

bget 客户端功能如下

- 直接指定 bagent 的 IP，执行命令
   e.g. ./bget -i 127.0.0.1 -p 8090 -k today
- 只需要制定 IP 和端口号，"http://" 和 url 完整路径会自动填充
- 输出内容包括：IO 错误，bagent 错误，命令 exit code，标准输出，标准错误输出

## commit note 2018.05.10
请求增加 bagent_ip 和 bagent_port 字段，可以经过 bproxy 请求 bagent

## commit note 2018.05.15
修改 proxy 方式。发起请求时必须制定 bagent 的 IP 和端口号。
