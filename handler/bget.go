package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type BgetWrapper struct {
	IP        string
	Port      int
	Proxy     string
	ProxyPort int
	Key       string
}

// 若 ProxyUrl 为空则直接向 TargetUrl 发起请求
type UrlWrapper struct {
	TargetUrl string
	ProxyUrl  string
}

type BgetResp struct {
	Code   int64  `json:"code"`
	Msg    string `json:"msg"`
	BKey   string `json:"bkey"`
	Osid   string `json:"osid"`
	Key    string `json:"key"`
	Stdout string `json:"stdout"`
	Stderr string `json:"stderr,omitempty"`
	Err    string `json:"err,omitempty"`
}

type BgetRespWrapper struct {
	SrcUrl string
	IOErr  string
	Resp   *BgetResp
}

func (w *BgetWrapper) Execute() {
	var url string
	var proxUrl string
	httpPre := "http://"

	url = fmt.Sprintf("%v%v:%v/bget", httpPre, w.IP, w.Port)
	if len(w.Proxy) != 0 {
		proxUrl = fmt.Sprintf("%v%v:%v", httpPre, w.Proxy, w.ProxyPort)
	}

	resp, err := Post(url, proxUrl, w.Key)
	respWrapper := BgetRespWrapper{}
	// 有 proxy 后，填入对应的 agent 的url
	// respWrapper.SrcUrl = url
	respWrapper.Resp = resp
	if resp == nil {
		respWrapper.Resp = &BgetResp{}
	}
	if err != nil {
		respWrapper.IOErr = err.Error()
	}
	//fmt.Printf("SrcUrl:%v\n", respWrapper.SrcUrl)
	fmt.Printf("IOError: %v\n", respWrapper.IOErr)
	fmt.Printf("Code: %v\n", respWrapper.Resp.Code)
	fmt.Printf("Msg: %v\n", respWrapper.Resp.Msg)
	fmt.Printf("Stdout:\n%v\n", respWrapper.Resp.Stdout)
	fmt.Printf("Stderr:\n%v\n", respWrapper.Resp.Stderr)
}

type BgetReq struct {
	Key        string `json:"key"`
	BagentIP   string `json:"bagent_ip"`
	BagentPort int    `json:"bagent_port"`
}

func Post(targetUrl string, proxyUrl string, key string) (resp *BgetResp, err error) {
	req := &BgetReq{}
	req.Key = key

	postJson, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	postReq, err := http.NewRequest("POST", targetUrl, bytes.NewBuffer(postJson))
	if err != nil {
		return nil, err
	}
	postReq.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	if len(proxyUrl) != 0 {
		proxyUrl, _ := url.Parse(proxyUrl)
		transport := &http.Transport{}
		transport.Proxy = http.ProxyURL(proxyUrl)
		client.Transport = transport
	}

	postResp, err := client.Do(postReq)
	if err != nil {
		return nil, err
	}
	defer postReq.Body.Close()

	body, _ := ioutil.ReadAll(postResp.Body)
	r := &BgetResp{}
	json.Unmarshal([]byte(body), r)
	return r, nil
}
